<?php

return [

    /*
    |---------------------------------------------------------------------
    | The default locale that will be used by the translation.
    |---------------------------------------------------------------------
    */
    'locale'        => 'tr',

    /*
    |---------------------------------------------------------------------
    | Supported languages for the translation
    |---------------------------------------------------------------------
    */
    'languages'     => [
        'tr'    => ['info' => 'Turkish', 'name' => 'Türkçe', 'locale' => 'tr_TR'],
        'en'    => ['info' => 'English', 'name' => 'English', 'locale' => 'en_EN']
    ],

    /*
    |---------------------------------------------------------------------
    | The default Timezone for your website.
    |---------------------------------------------------------------------
    */
    'timezone'      => 'Europe/Istanbul',

    /*
    |---------------------------------------------------------------------
    | Session configuration parameters
    |---------------------------------------------------------------------
    */
    'session'		=> [
        'encryption_key'			=> 'BFnSycxGnf',
        'cookie_httponly'			=> true,
        'use_only_cookies'			=> true,
        'lifetime'					=> 3600,
    ],

    /*
    |---------------------------------------------------------------------
    | Cookie configuration parameters
    |---------------------------------------------------------------------
    */
    'cookie'		=> [
        'encryption_key'			=> 'N5dqU42az5',
        'cookie_security'			=> true,
        'http_only'					=> true,
        'secure'					=> false,
        'separator'					=> '--',
        'path'						=> '/',
        'domain'					=> '',
    ],

    /*
    |---------------------------------------------------------------------
    | Simple http cache configuration parameters
    |---------------------------------------------------------------------
    */
    'cache'			=> [
		'path'						=> 'Storage' . DIRECTORY_SEPARATOR . 'Cache',
		'extension'					=> '.cache',
		'expire'					=> 604800,
	],

    /*
    |---------------------------------------------------------------------
    | SMTP mail configuration parameters
    |---------------------------------------------------------------------
    */
    'email'			=> [
        'server'					=> 'smtp.mailtrap.io',
        'port'						=> 2525,
        'username'					=> '89dd433039e095',
        'userpass'					=> '9dbfa1294bfff3',
        'charset'					=> 'utf-8',
    ],
];