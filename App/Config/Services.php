<?php

return [

    /*
    |--------------------------------------------------------------------------
    | The Registered Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application.
    |
    */
    'providers' => [

        /*
         * Titan Framework service providers.
         */
        Titan\Libraries\Router\RouterServiceProvider::class,
        Titan\Libraries\View\ViewServiceProvider::class,
        Titan\Libraries\Hash\HashServiceProvider::class,
        Titan\Libraries\Log\LogServiceProvider::class,
        Titan\Libraries\Session\SessionServiceProvider::class,
        Titan\Libraries\Cookie\CookieServiceProvider::class,
        Titan\Libraries\Http\Request\RequestServiceProvider::class,
        Titan\Libraries\Http\Response\ResponseServiceProvider::class,
        Titan\Libraries\Http\Curl\CurlServiceProvider::class,
        Titan\Libraries\Http\Restful\RestfulServiceProvider::class,
        Titan\Libraries\Cache\CacheServiceProvider::class,
        Titan\Libraries\Database\ModelServiceProvider::class,
        Titan\Libraries\Database\DBServiceProvider::class,
        Titan\Libraries\Benchmark\BenchmarkServiceProvider::class,
        Titan\Libraries\Language\LanguageServiceProvider::class,
        Titan\Libraries\Date\DateServiceProvider::class,
        Titan\Libraries\Validation\ValidationServiceProvider::class,
        Titan\Libraries\Mail\MailServiceProvider::class,
        Titan\Libraries\Uri\UriServiceProvider::class,

        /*
         * Third party service providers.
         */

    ],

    /*
    |--------------------------------------------------------------------------
    | Aliases for registered providers
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application is
    | started.
    |
    */
    'facades' => [
        'Route'     => Titan\Facades\Router::class,
        'View'      => Titan\Facades\View::class,
        'Hash'      => Titan\Facades\Hash::class,
        'Log'       => Titan\Facades\Log::class,
        'Session'   => Titan\Facades\Session::class,
        'Cookie'    => Titan\Facades\Cookie::class,
        'Request'   => Titan\Facades\Request::class,
        'Response'  => Titan\Facades\Response::class,
        'Curl'      => Titan\Facades\Curl::class,
        'Restful'   => Titan\Facades\Restful::class,
        'Cache'     => Titan\Facades\Cache::class,
        'Model'     => Titan\Facades\Model::class,
        'DB'        => Titan\Facades\DB::class,
        'Benchmark' => Titan\Facades\Benchmark::class,
        'Language'  => Titan\Facades\Language::class,
        'Date'      => Titan\Facades\Date::class,
        'Validation'=> Titan\Facades\Validation::class,
        'Mail'      => Titan\Facades\Mail::class,
        'Uri'       => Titan\Facades\Uri::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | The Application's Middleware stack
    |--------------------------------------------------------------------------
    |
    | These middleware are run during every request to your application.
    |
    */
    'middleware' => [
        'default'   => [],
        'manual'    => [
            'SampleMiddleware' => App\Middlewares\SampleMiddleware::class
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Titan Any Application Services
    |--------------------------------------------------------------------------
    |
    | This array of services will be registered when this application is
    | started. It is a great spot to register your custom services.
    |
    */
    'services' => [
        'SampleService' => App\Services\SampleService::class
    ]

];
